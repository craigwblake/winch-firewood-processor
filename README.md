# Winch Firewood Processor

A firewood processor with a sliding chute that can accept full length trees from the ground level without the use of another machinery needed.
It targets customers that do not have a tractor and would like to process there own firewood